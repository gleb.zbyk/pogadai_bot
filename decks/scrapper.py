import requests
from bs4 import BeautifulSoup

list_for_random_choice = []

for n in range(1, 79):

    url = 'https://www.taro.lv/ru/page/gallery/78_dverej/door_' + str(n)

    response = requests.get(url)

    soup = BeautifulSoup(response.content, 'html.parser')
    card_name = str(soup.find('h1'))
    card_name = card_name[4:-5]
    with open('post_for_poster.txt', 'a') as file:
        file.writelines((card_name,'\n'))

    images = soup.find_all('img', attrs={"width": "161"})
    # names = soup.find_all('div', attrs={"class": "card-description"})

    f = open(card_name + '.txt', 'w')
    for image in images:
        image_src = 'http://www.taro.lv/' + image['src'] + '\n'
        with open(card_name + '.txt', 'a') as file:
            file.writelines(image_src)

    list_for_random_choice.append(card_name)
print(list_for_random_choice)
# print(images, '\n', names)
