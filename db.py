from sqlalchemy import insert, MetaData, Table, String, Integer, Column, Boolean, Text, ForeignKey, create_engine, \
    DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

engine = create_engine("sqlite:///test1.sqlite", echo=True)

base = declarative_base()

metadata = MetaData()

users = Table(
    'users', metadata,
    Column('id', Integer, primary_key=True),
    Column('tg_id', Integer, ),
    Column('nickname', String(200)),
    Column('created_on', DateTime(), default=datetime.now),
    # UniqueConstraint('id', 'data', sqlite_on_conflict='IGNORE')
)

msg_of_user = Table(
    'msg_of_user', metadata,
    Column('nickname', ForeignKey('users.nickname')),
    Column('msg', String(200))
)

card_of_the_day = Table(
    'card_of_the_day', metadata,
    Column('nickname', ForeignKey('users.nickname')),
    Column('msg', Text),
    Column('card_of_day', Text)
)

# newsletter = Table(
#     'newsletter', metadata,
#     Column('tg_id', ForeignKey('users.tg_id')),
#     Column('isactive', Boolean, default=True)
# )

secret_game_status = Table(
    'secret_game_status', metadata,
    Column('tg_id', ForeignKey('users.tg_id')),
    Column('isactive', Boolean, default=False)
)

metadata.create_all(engine)
