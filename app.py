# coding=<utf-8>
import os
import telebot
from telebot import types
import random
import urllib.request
from sqlalchemy import insert
from sqlalchemy.orm import sessionmaker
from db import engine, msg_of_user, users, secret_game_status
import time

from config import TG_TOKEN

bot = telebot.TeleBot(TG_TOKEN)


@bot.message_handler(commands=['start'])
def buttons(message):
    if not table_user_check(message.chat.id):
        user_add_to_user_table(message.from_user.username, message.chat.id)

    markup = types.ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True, resize_keyboard=True)
    itembtn1 = types.KeyboardButton('/card_of_the_day')
    markup.add(itembtn1)
    send_msg = f'Привет, {message.from_user.first_name}.\n' \
               f'Я помогу тебе достать карту дня, чтобы\n' \
               f'твой день был чуточку проще и спокойнее.\n' \
               f'Просто нажми /card_of_the_day'
    bot.reply_to(message, send_msg, reply_markup=markup)


@bot.message_handler(commands=['card_of_the_day'])
def send_card_of_the_day(message):
    markup = types.ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True, resize_keyboard=True)
    card_of_day = random.choice(list(open('decks/post_for_poster.txt', encoding='windows-1251')))
    card_pick_url = card_deck_pic(card_of_day)
    card_desc = str(card_description(card_of_day))
    card_img_path = get_temp_img_from_url(card_pick_url, card_of_day)

    bot.reply_to(message, 'Мешаю колоду')

    card_img = open(card_img_path, 'rb')
    # print(card_img)
    bot.send_photo(message.chat.id, card_img, caption=card_desc)
    data = '@' + message.from_user.username + ' ' + card_of_day + ' ' + card_desc

    if str(message.chat.id) == "183241723":  # nata - 183241723 #ya - 101080370

        time.sleep(3)

        img_vedro = open('static/420.jpg', 'rb')

        prepare_msg = 'Забивай скорее, сегодня будет твой день, Красотка🌞🌞🌞'

        bot.send_photo(183241723, img_vedro, caption=prepare_msg)
        # bot.send_message(101080370, 'Забивай скорее, сегодня будет твой день, Красотка🌞🌞🌞')
        itembtn1 = types.KeyboardButton('/go_dunem')
        # itembtn2 = types.KeyboardButton('Не курю, прости')
        markup.add(itembtn1)
        bot.send_message(183241723, 'Погнали дуть?', reply_markup=markup)
    else:
        bot.reply_to(message, f'Хочешь получать карту дня каждый день автоматически?'
                              f'Тогда нажми на /sub_activate')

    bot.send_message(101080370, data)


@bot.message_handler(commands=['sub_activate420'])
def add_to_everyday_card_newsletter(message):
    if table_secret_game_status_check(message.chat.id):
        user_add_to_secret_game_status_table(message.chat.id)
        bot.reply_to(message, 'Поздравляем, теперь ты в игре')
        bot.send_message(message.chat.id, 'Отныне ты можешь отправлять кружочек боту как ты дуешь, '
                                          'а он в ответ тебе отправит кого-то из этого чата, кто тоже участвует'
                                          ' в секретной рассылке. Попробуй')

    else:
        bot.reply_to(message, 'Ваша рассылка уже включена')


@bot.message_handler(commands=['go_dunem'])
def send_420_video(message):
    bot.reply_to(message, 'ща-ща-ща, подожди')
    video = open('static/that_time.mp4', 'rb')
    bot.send_video(message.chat.id, video, supports_streaming=True)


# TODO рассылка раз в сутки
def send_news_from_admin(message):
    # print(message.text[0:4])
    if str(message.chat.id) == '101080370' and message.text[0:4] == 'test':
        # достать имена и статус актив из бд
        print(message.text[6:])


# TODO Комплименто-бот

# TODO всё получится-бот


@bot.message_handler(commands=['chei_krim'])
def chei_krim(message):
    if str(message.chat.id) == "183241723":
        answer = f"Наш крым, ну ты чего?\n" \
                 f"А бот твой - @natactica\n"
        bot.send_message(message.chat.id, answer)
    else:
        answer = f'Не твой бот, прости' \
                 f'И крым не твой\n' \
                 f'Бот принадлежит Богине @natactica\n' \
                 f'Муцураеву Акбар!'
        bot.send_message(message.chat.id, answer)


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    send_news_from_admin(message)
    # print(message.text)
    # if str(message.chat.id) == '101080370':
    #     print('420_time')
    if message.text == 'Не курю, прости':
        video = open('static/galaxy.mp4', 'rb')
        bot.send_video(message.chat.id, video, supports_streaming=True)

    add_user_msg_to_table(message.from_user.username, str(message.text))
    # bot.reply_to(message, message.text)
    a = str(message.chat.id)
    print('@' + message.from_user.username + ' ' + a + ' ' + message.text)


@bot.message_handler(content_types=['video_note'])
def video_note_test(message):
    if table_secret_game_status_check(message.chat.id):

        file_info = bot.get_file(message.video_note.file_id)
        downloaded_file = bot.download_file(file_info.file_path)
        n = random.randint(0, 69420)
        with open(f'420/{message.chat.id}_{n}.mp4', 'wb') as new_file:
            new_file.write(downloaded_file)
        #time.sleep(4)
        #bot.forward_message(message.chat.id, message.chat.id, 1397)
        url_video_420 = random.choice(os.listdir("420\\"))  # change dir name to whatever
        video = open('420\\'+url_video_420, 'rb')
        bot.send_video_note(message.chat.id, video)
    else:
        bot.reply_to(message, "А ты не в клубе")



def get_temp_img_from_url(url, cardname):
    card = cardname[0:-1]
    urllib.request.urlretrieve(url, f'temp/{card}.jpg')
    img_path = f'temp/{card}.jpg'
    return img_path


def card_deck_pic(card):
    card_img = os.path.abspath('decks/' + card[:-1] + '.txt')
    return random.choice(list(open(card_img)))


def card_description(card):
    inp = open("decks/cards_description.txt", encoding='utf-8').readlines()
    card = card[0:-2]
    for i in inp:
        if card in i:
            return i[3:]


def table_user_check(tg_id):
    user_in_list = False
    Session = sessionmaker(bind=engine)
    session = Session()

    result = session.query(users).all()
    for row in result:
        if row.tg_id == tg_id:
            user_in_list = True
    if user_in_list == False:
        return False
    else:
        return True


def user_add_to_user_table(nickname, tg_id):
    with engine.connect() as conn:
        add_user = conn.execute(
            insert(users),
            [
                {"nickname": nickname, "tg_id": tg_id},

            ]
        )
    return 'хуй'


def table_secret_game_status_check(tg_id):
    user_in_list = False
    Session = sessionmaker(bind=engine)
    session = Session()

    result = session.query(secret_game_status).all()
    for row in result:
        if row.tg_id == tg_id:
            user_in_list = True
    if not user_in_list:
        return False
    else:
        return True


def user_add_to_secret_game_status_table(tg_id):
    with engine.connect() as conn:
        add_user = conn.execute(
            insert(secret_game_status),
            [
                {"isactive": True, "tg_id": tg_id},

            ]
        )
    return 'хуй'


def secret_game_status_change(tg_id):
    Session = sessionmaker(bind=engine)
    session = Session()
    result1 = session.query(secret_game_status.col1.label(tg_id))
    print(result1)

    with engine.connect() as conn:
        add_user = conn.execute(
            insert(secret_game_status),
            [
                {"isactive": True, "tg_id": tg_id},

            ]
        )
    return True


def secret_game_status_check(tg_id):
    Session = sessionmaker(bind=engine)
    session = Session()

    result = session.query(secret_game_status).all()
    for row in result:
        if row.tg_id == tg_id and row.isactive == True:
            return True

        return False


def add_user_msg_to_table(nickname, user_msg):
    with engine.connect() as conn:
        add_user_msg = conn.execute(
            insert(msg_of_user),
            [
                {"nickname": nickname, "msg": user_msg}

            ]
        )
    return 'хуй'


if __name__ == "__main__":
    bot.infinity_polling()
